
#this is based on tianon/latex, but updated to newer debian

FROM debian:stable-slim

RUN apt-get update
RUN apt-get install -y \
	biber latexmk make \
	texlive-latex-extra \
	texlive-fonts-extra \
	texlive-bibtex-extra \
	texlive-science texlive-publishers texlive-pictures \
	texlive-formats-extra

RUN apt-get install -y \
	r-base r-recommended

RUN R -e "install.packages(c('devtools', 'cowplot', 'ggplot2', 'tidyverse', 'reshape2', 'rmarkdown'))"

RUN rm -rf /var/lib/apt/lists/*
